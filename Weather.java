import java.util.Scanner;

public class Weather{
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Какая температура на улице?");
		int temp = sc.nextInt();
		sc.nextLine();
		System.out.println("На следующие вопросы нужно отвечать только Да или Нет.");
		System.out.println("Идет ли на улице дождь?");
		String rain_inf = sc.nextLine();
		System.out.println("Есть ли на улице ветер?");
		String wind_inf = sc.nextLine();
		String yes = "да";
		String no = "нет";
		if((yes.equalsIgnoreCase(wind_inf) | no.equalsIgnoreCase(wind_inf)) & (yes.equalsIgnoreCase(rain_inf) | no.equalsIgnoreCase(rain_inf))) {
			if(temp <= -18){
		        System.out.println("На улице очень холодно, поэтому лучше остаться дома.");
		    }else if((temp > -18 && temp <= 0 && no.equalsIgnoreCase(wind_inf)) || (temp > 0 && temp <= 10 && no.equalsIgnoreCase(rain_inf) && yes.equalsIgnoreCase(wind_inf))){
		        System.out.println("Погода приемлема для прогулки, если тепло одеться.");
		    }else if(temp > -18 && temp <= 0 && yes.equalsIgnoreCase(wind_inf)){
		        System.out.println("Низкая температура и ветер - не лучшая комбинация. Лучше остаться дома.");
		    }else if(temp > 0 && no.equalsIgnoreCase(wind_inf) && no.equalsIgnoreCase(rain_inf)){
		        System.out.println("Погода приемлема для прогулки.");
		    }else if(temp > 0 && temp <= 10 && no.equalsIgnoreCase(wind_inf) && yes.equalsIgnoreCase(rain_inf)){
		        System.out.println("Погода приемлема для прогулки, если не забыть взять зонт.");
		    }else if(yes.equalsIgnoreCase(wind_inf) && yes.equalsIgnoreCase(rain_inf)){
		        System.out.println("Ветер и дождь - не лучшая комбинация при любой температуре. Лучше остаться дома.");
		    }else if(temp > 10 && yes.equalsIgnoreCase(wind_inf) && no.equalsIgnoreCase(rain_inf)){
		        System.out.println("Погода приемлема для прогулки, несмотря на  ветер.");
		    }else if(temp > 10 && yes.equalsIgnoreCase(rain_inf) && no.equalsIgnoreCase(wind_inf)){
		        System.out.println("Погода приемлема для прогулки, несмотря на дождь. Главное не забыть взять зонт!");
		    }
		}else{
			System.out.println("Ошибка ввода данных.");
		}
	} 
}